#version 450 core

layout(location = 0) out vec4 texture_ids;
layout(location = 1) out vec4 texture_coord;


in vec2 texture_coords;

uniform float object_index;
uniform sampler2D noise_texture;

uniform sampler2D accumulation_0;
uniform sampler2D accumulation_1;
uniform sampler2D accumulation_2;
uniform sampler2D accumulation_3;

uniform float time_bias;

//out vec2 test_change;

float bias = 0.15f;

uniform float sampleOffset = 1.0;

void main() {
    
//    vec2 test_change = dFdy(texture_coords) * 10;
//    vec2 test_changex = dFdx(texture_coords) * 10;
//    
//    texture2 = vec4(test_changex, 0, 0);
    //texture2 = vec4(texture_coords.x -test_changex[0], texture_coords.y - test_change[0], texture_coords.x+test_changex[0], texture_coords.y+test_change[0]);
    
    float idx = (object_index+1) / 16;
    vec2 global_tex = vec2((gl_FragCoord.x / 1024) + time_bias, (gl_FragCoord.y / 1024) + time_bias);
    
    float noise = texture(noise_texture, global_tex).r;
    
    float accumulation = 1.0f;
    
    texture_ids = vec4(object_index+1, noise, accumulation,0);

    float off = 10.0f;
    
    vec2 p1 = interpolateAtOffset(texture_coords, vec2(-off,-off));
    vec2 p2 = interpolateAtOffset(texture_coords, vec2(off,off));

    vec2 pmin = vec2(min(p1.x, p2.x), min(p1.y, p2.y));
    vec2 pmax = vec2(max(p1.x, p2.x), max(p1.y, p2.y));

    texture_coord = vec4(pmin.x, pmin.y, pmax.x, pmax.y);
    
    
}  