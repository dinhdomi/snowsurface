#version 410 core

out vec4 FragColor;

in vec3 tesNormal;
in vec3 tesPosition;
in vec2 tesTexCoord;
in vec4 tesLightSpaceFragPosition;
in mat3 tesTBN;
in vec3 tesTangentFragPos;
in float tesselation_level;

uniform mat4 view;
uniform vec3 camera_position;
uniform sampler2D texture_diff;
uniform sampler2D texture_shadow1;
uniform sampler2D texture_shadow2;
uniform sampler2D texture_noise;
uniform sampler2D texture_acc;
uniform sampler2D texture_normal;

uniform float noise_number;

float shadowCalc(vec4 fragPos) {

    vec4 shadowPos = fragPos / fragPos.w;

    float bias =  0.0012;
    float visibility = 1.0f;
    if ((shadowPos.x < 0 || shadowPos.x > 1 || shadowPos.y < 0 || shadowPos.y > 1 || shadowPos.z < 0 || shadowPos.z > 1)){
        visibility = 1.0f;
    }else{
        float shadowDepth = texture(texture_shadow1, shadowPos.xy).r;
        if(shadowDepth<shadowPos.z-bias)
        visibility = 0.0f;
    }
    return visibility;
}

vec4 pointLight(vec4 diffuse, vec3 frag_pos, vec3 normals) {

    vec3 normal = normalize(normals * 2.0f - 1.0f);
    
    vec3 ret = vec3(0.0);
    vec3 light_dir;
    vec3 light_pos;

    
    light_pos = tesTBN * vec3(0.0f, 10.0f, 0.0f);
    light_dir = normalize(light_pos-frag_pos);
   

    float diff = max(dot(light_dir, normal), 0.0);

    vec3 reflectDir = reflect(-light_dir, normal);
    vec3 viewer_dir;

    viewer_dir = normalize(tesTBN * camera_position - frag_pos);

    float spec = pow(max(dot(viewer_dir, reflectDir), 0.0), 2.0f);

    float distance;
    
    distance = length(frag_pos - light_pos);

    float atten = 1.0 / ( 1.0 + 0.0007 * distance + 0.00002 * distance * distance );

    vec3 light = vec3(255, 255, 255) / 255.0;
    ret += (vec3(0.2f) * light) * atten;
    ret += ( diffuse.rgb * diff * light) * atten;
    ret += ( diffuse.rgb * spec * light) * atten;

    return vec4(ret , 1.0);
}

vec4 directionalLight(vec3 vertexPosition, vec3 vertexNormal, float shadow) {

    vec3 normal = normalize(vertexNormal * 2.0f - 1.0f);
    vec3 light_position = tesTBN * vec3(5.0f, 10.0f, 0.0f);
    vec3 ret = vec3(0.0);

    vec3 L = normalize(light_position);
    
    vec3 R = reflect(-L, normal);
    vec3 V = normalize(tesTBN * camera_position-vertexPosition);

    ret += vec3(1.0f) * vec3(texture(texture_diff, tesTexCoord)) * max(dot(-L, normal),0.0);

    ret += vec3(0.5f, 0.5f, 0.5f) * vec3(1.0f, 1.0f, 1.0f)  * pow(max(dot(V, R), 0.0), 32.0f);

    ret += vec3(1.0f, 0.5f, 0.31f) * texture(texture_diff, tesTexCoord).rgb;

    return vec4(ret, 1.0) ;
}

void main()
{
    vec3 normal = texture(texture_normal, tesTexCoord).rgb;
    vec3 globalAmbientLight = vec3(0.4f);
    // 
//    FragColor =(texture(texture_noise, TexCoord));
    float shadow = shadowCalc(tesLightSpaceFragPosition);

    vec4 acc =  texture(texture_acc, tesTexCoord);
    float snowRatio = min(acc.x, 0.250f) * 4.0f;
    
    vec4 color = (1 - snowRatio) * texture(texture_diff, tesTexCoord) + snowRatio * vec4(1.0f);
//    vec4 color = vec4(tesselation_level);
//    color.a = 1.0f;
    
    
    //vec3 n = (1 - snowRatio) * tesNormal + snowRatio * normal;
    FragColor = color;
    
    
    FragColor = pointLight(color, tesTangentFragPos, normal) * color;
//
    FragColor.a = 1.0f;
//    
    
    

    
    
//    
   
    
    
    //FragColor = vec4(TexCoord, 0.0f, 1.0f);
}