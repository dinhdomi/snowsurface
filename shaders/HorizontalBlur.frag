#version 330 core

in vec2 texture_coordinates;

//uniform float offset[5] = float[]( -3.2307, -1.3846, 0.0, 1.3846, 3.2307 );
//uniform float weight[5] = float[]( 0.0702, 0.3162, 0.2270, 0.3162, 0.0702 );

uniform float offset[5] = float[]( 0.0, 1.0, 2.0, 3.0, 4.0 );
uniform float weight[5] = float[]( 0.2270270270, 0.1945945946, 0.1216216216, 0.0540540541, 0.0162162162 );

uniform float off = 1.0/1024.0;

uniform sampler2D pre_texture;
out vec4 resulting_color;

void main() {

    resulting_color = texture2D( pre_texture, texture_coordinates ) * weight[0];
    
    for (int i=1; i<5; i++) {
        resulting_color += texture2D( pre_texture, ( texture_coordinates + vec2(offset[i] * off, 0.0) ) ) * weight[i];
        resulting_color += texture2D( pre_texture, ( texture_coordinates - vec2(offset[i] * off, 0.0) ) ) * weight[i];
    }
}