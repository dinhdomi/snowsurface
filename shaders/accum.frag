#version 330 core


layout(location = 0) out vec4 acc_0;
layout(location = 1) out vec4 acc_1;
layout(location = 2) out vec4 acc_2;
layout(location = 3) out vec4 acc_3;
layout(location = 4) out vec4 acc_4;
layout(location = 5) out vec4 acc_5;
layout(location = 6) out vec4 acc_6;


in float accumulation;
in float texID;

//out float accumulation_output;

out vec4 FragColor;
uniform sampler2D occlusion2;

void main() {
    
//    
     vec2 X = vec2(0.3105f, 0.3633f);
     vec2 Y = vec2(0.3838f, 0.0605f);
//    
    vec2 curr_coord = gl_FragCoord.xy / 1024.0f;
//    
    acc_0 = vec4(vec3(texID), 1.0f);
    if (texID == 1.0f) {
            acc_0 = vec4(vec3(accumulation), 1.0f);   
        }
        if (texID == 2.0f) { // 
            acc_1 = vec4(vec3(accumulation), 1.0f);
        }
        if (texID == 3.0f) {
            acc_2 = vec4(vec3(accumulation), 1.0f);
        }
    
    if (texID == 4.0f) {
        acc_3 = vec4(vec3(accumulation), 1.0f);
    }
    
    
//    if (ID >= 0.0f && ID <= 0.1f) {
//        acc_0 = vec4(1.0f, 1.0f, 1.0f, 1.0f);   
//    }
//    if (ID >= 0.9f && ID <= 1.1f) {
//        acc_1 = vec4(1.0f, 1.0f, 1.0f, 1.0f);
//    }
//    if (ID >= 1.9f && ID <= 2.1f) {
//        acc_2 = vec4(1.0f, 1.0f, 1.0f, 1.0f);
//    }
    //accumulation_output = 0.4f;
}