#version 460 core

in vec2 texture_coordinates;

uniform sampler2D accumulation;

out vec4 resulting_color;

void main() {

//    float pixel_curr = texture(accumulation, texture_coordinates).r;
//    float pixel_BL = textureOffset(accumulation, texture_coordinates, ivec2(-1, -1)).r;
//    float pixel_BM = textureOffset(accumulation, texture_coordinates, ivec2(0, -1)).r;
//    float pixel_BR = textureOffset(accumulation, texture_coordinates, ivec2(1, -1)).r;
//    float pixel_MR = textureOffset(accumulation, texture_coordinates, ivec2(1, 0)).r;
//    float pixel_TR = textureOffset(accumulation, texture_coordinates, ivec2(1, 1)).r;
//
//    float pixel_TM = textureOffset(accumulation, texture_coordinates, ivec2(0, 1)).r;
//    float pixel_TL = textureOffset(accumulation, texture_coordinates, ivec2(-1, 1)).r;
//    float pixel_ML = textureOffset(accumulation, texture_coordinates, ivec2(-1, 0)).r;
//    
//    float values[8] = float[8](pixel_BL, pixel_BM, pixel_BR, pixel_MR, pixel_TR, pixel_TM, pixel_TL, pixel_ML);
//    
//    float current_highest = pixel_curr;
//    
//    for(int i = 0; i < 8; i++ ) {
//        
//        if (values[i] > current_highest) {
//            current_highest = values[i];
//        }
//    }
    
    float current_highest = 0;
    
    for(int y = -3; y <= 3;y++) {
        
        for(int x = -3; x <= 3; x++) {
            float curr = textureOffset(accumulation, texture_coordinates, ivec2(x, y)).r;
            
            if (curr > current_highest) {
                current_highest = curr;
            }
        }
    }
    
    resulting_color = vec4(current_highest);
    resulting_color.a = 1;
    
}