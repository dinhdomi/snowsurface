#include "Shader.h"

Shader::Shader(std::string vertexPath, std::string fragmentPath) {

    std::string vertex_code;
    std::string fragment_code;

    std::ifstream vertex_shader_file;
    std::ifstream fragment_shader_file;

    vertex_shader_file.open(vertexPath.c_str());
    fragment_shader_file.open(fragmentPath.c_str());

    std::cout << vertexPath.c_str() << std::endl;
    std::stringstream vShaderStream, fShaderStream;
    // read file's buffer contents into streams
    vShaderStream << vertex_shader_file.rdbuf();
    fShaderStream << fragment_shader_file.rdbuf();		
    // close file handlers
    vertex_shader_file.close();
    fragment_shader_file.close();
    // convert stream into string
    vertex_code   = vShaderStream.str();
    fragment_code = fShaderStream.str();
    CHECK_GL_ERROR();
    const char* vShaderCode = vertex_code.c_str();
    const char* fShaderCode = fragment_code.c_str();

    GLuint vertex, fragment;

    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vShaderCode, nullptr);
    glCompileShader(vertex);
    CHECK_GL_ERROR();
    checkCompileErrors(vertex, "VERTEX");
    // fragment Shader
    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fShaderCode, NULL);
    glCompileShader(fragment);
    checkCompileErrors(fragment, "FRAGMENT");
    
    // shader Program
    program_id = glCreateProgram();
    glAttachShader(program_id, vertex);
    glAttachShader(program_id, fragment);
    glLinkProgram(program_id);
    checkCompileErrors(program_id, "PROGRAM");
    // delete the shaders as they're linked into our program now and no longer necessary
    glDeleteShader(vertex);
    glDeleteShader(fragment);
    CHECK_GL_ERROR();
}

Shader::Shader(std::string vertexPath, std::string fragmentPath, std::string geometryPath) {

    std::string vertex_code;
    std::string fragment_code;
    std::string geometry_code;

    std::ifstream vertex_shader_file;
    std::ifstream fragment_shader_file;
    std::ifstream geometry_shader_file;

    vertex_shader_file.open(vertexPath.c_str());
    fragment_shader_file.open(fragmentPath.c_str());
    geometry_shader_file.open(geometryPath.c_str());

    std::stringstream vShaderStream, fShaderStream, gShaderStream;
    // read file's buffer contents into streams
    vShaderStream << vertex_shader_file.rdbuf();
    fShaderStream << fragment_shader_file.rdbuf();
    gShaderStream << geometry_shader_file.rdbuf();
    
    // close file handlers
    vertex_shader_file.close();
    fragment_shader_file.close();
    geometry_shader_file.close();
    
    // convert stream into string
    vertex_code   = vShaderStream.str();
    fragment_code = fShaderStream.str();
    geometry_code = gShaderStream.str();
    
    CHECK_GL_ERROR();
    const char* vShaderCode = vertex_code.c_str();
    const char* fShaderCode = fragment_code.c_str();
    const char* gShaderCode = geometry_code.c_str();

    GLuint vertex, fragment, geometry;

    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vShaderCode, nullptr);
    glCompileShader(vertex);
    CHECK_GL_ERROR();
    checkCompileErrors(vertex, "VERTEX");
    
    // fragment Shader
    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fShaderCode, NULL);
    glCompileShader(fragment);
    checkCompileErrors(fragment, "FRAGMENT");

    geometry = glCreateShader(GL_GEOMETRY_SHADER);
    glShaderSource(geometry, 1, &gShaderCode, NULL);
    glCompileShader(geometry);
    checkCompileErrors(geometry, "GEOMETRY");
    
    // shader Program
    program_id = glCreateProgram();
    glAttachShader(program_id, vertex);
    glAttachShader(program_id, fragment);
    glAttachShader(program_id, geometry);
    
    glLinkProgram(program_id);
    checkCompileErrors(program_id, "PROGRAM");
    // delete the shaders as they're linked into our program now and no longer necessary
    glDeleteShader(vertex);
    glDeleteShader(fragment);
    glDeleteShader(geometry);
    CHECK_GL_ERROR();
}

Shader::Shader(std::string vertexPath, std::string fragmentPath, std::string tessControlPath,
    std::string tessEvalPath) {

    std::string vertex_code;
    std::string fragment_code;
    std::string tessControl_code;
    std::string tessEval_code;
    
    std::ifstream vertex_shader_file;
    std::ifstream fragment_shader_file;
    std::ifstream geometry_shader_file;
    std::ifstream tess_control_shader_file;
    std::ifstream tess_eval_shader_file;
    
    vertex_shader_file.open(vertexPath.c_str());
    fragment_shader_file.open(fragmentPath.c_str());
    tess_control_shader_file.open(tessControlPath.c_str());
    tess_eval_shader_file.open(tessEvalPath.c_str());

    std::stringstream vShaderStream, fShaderStream, tcsShaderStream, tesShaderStream;
    // read file's buffer contents into streams
    vShaderStream << vertex_shader_file.rdbuf();
    fShaderStream << fragment_shader_file.rdbuf();
    tcsShaderStream << tess_control_shader_file.rdbuf();
    tesShaderStream << tess_eval_shader_file.rdbuf();
    
    // close file handlers
    vertex_shader_file.close();
    fragment_shader_file.close();
    geometry_shader_file.close();
    tess_control_shader_file.close();
    tess_eval_shader_file.close();
    
    // convert stream into string
    vertex_code   = vShaderStream.str();
    fragment_code = fShaderStream.str();
    tessControl_code = tcsShaderStream.str();
    tessEval_code = tesShaderStream.str();
    
    CHECK_GL_ERROR();
    const char* vShaderCode = vertex_code.c_str();
    const char* fShaderCode = fragment_code.c_str();
    const char* tcsControlCode = tessControl_code.c_str();
    const char* tesEvalCode = tessEval_code.c_str();

    GLuint vertex, fragment, geometry, tesControl, tesEval;

    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vShaderCode, nullptr);
    glCompileShader(vertex);
    CHECK_GL_ERROR();
    checkCompileErrors(vertex, "VERTEX");
    
    // fragment Shader
    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fShaderCode, NULL);
    glCompileShader(fragment);
    checkCompileErrors(fragment, "FRAGMENT");

    tesControl = glCreateShader(GL_TESS_CONTROL_SHADER);
    glShaderSource(tesControl, 1, &tcsControlCode, NULL);
    glCompileShader(tesControl);
    checkCompileErrors(tesControl, "TESSELATION CONTROL");

    tesEval = glCreateShader(GL_TESS_EVALUATION_SHADER);
    glShaderSource(tesEval, 1, &tesEvalCode, NULL);
    glCompileShader(tesEval);
    checkCompileErrors(tesEval, "TESSELATION EVALUATION");

    
    // shader Program
    program_id = glCreateProgram();
    glAttachShader(program_id, vertex);
    glAttachShader(program_id, fragment);
    glAttachShader(program_id, tesControl);
    glAttachShader(program_id, tesEval);
    
    glLinkProgram(program_id);
    checkCompileErrors(program_id, "PROGRAM");
    // delete the shaders as they're linked into our program now and no longer necessary
    glDeleteShader(vertex);
    glDeleteShader(fragment);
    glDeleteShader(tesControl);
    glDeleteShader(tesEval);
    
    CHECK_GL_ERROR();
}
