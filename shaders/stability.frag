#version 330 core

in vec3 oPosition;
in vec3 oNormals;
in vec3 oTangent;
in vec2 oTexture_coordinates;


uniform sampler2D accumulation_map;
uniform sampler2D diffuse;

out vec4 result;


void main() {

//    vec4 acc = texture(accumulation_map, oTexture_coordinates);
//
//    vec3 down = vec3(0,1,0);
//
//    float maximumSnow = dot(normalize(oNormals), down);
//
//    float uppBound = 0.2;
//
//    maximumSnow = maximumSnow + (uppBound*0.5);
//
//    if(maximumSnow <= uppBound)
//    {
//
//        if(maximumSnow < 0)
//        {
//            maximumSnow = 0;
//        }
//
//        maximumSnow = 1.0 - (maximumSnow / uppBound);
//
//
//
//        result = result - (0.01 * maximumSnow);
//        //result.r = 0.5f;
//        //acc = acc - (maximumSnow);
//
//        result.r = max(0, result.r);
//        result.g = max(0, result.r);
//        result.b = max(0, result.r);
//        result.a = 1.0f;
//    }
    
//    
//    result.r = acc.r;
//    result = vec4(result.r);
//    result.a = 1.0f;
    
    float accumulation = texture(accumulation_map, oTexture_coordinates).r;

    vec3 up_vector = vec3(0, 1, 0);

    float NY = dot(normalize(oNormals), up_vector);

    float max_height = 0.5f;

    float falloff_const = accumulation - NY * max_height;

    float res = accumulation;

    if (falloff_const >= 0) {
        res = accumulation - falloff_const * 0.2f;
    }


    result = vec4(res);
    result.a = 1.0f;
}