﻿#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include <glad/glad.h>
#include "../Core/Statics.h"
class Shader {
public:
    Shader(std::string vertexPath, std::string fragmentPath);
    Shader(std::string vertexPath, std::string fragmentPath, std::string geometry);
    Shader(std::string vertexPath, std::string fragmentPath, std::string tessControlPath, std::string tessEvalPath);
    
    GLuint program_id;

    GLint posLocation;       // = -1;
    GLint colorLocation;     // = -1;
    GLint normalLocation;    // = -1;
    GLint texCoordLocation;
    
    GLint PVMmatrixLocation;    // = -1;
    GLint VmatrixLocation;      // = -1;  view/camera matrix
    GLint MmatrixLocation;      // = -1;  modeling matrix
    GLint normalMatrixLocation;

    GLint diffuseLocation;    // = -1;
    GLint ambientLocation;    // = -1;
    GLint specularLocation;   // = -1;
    GLint shininessLocation;  // = -1;
    // texture
    GLint useTextureLocation; // = -1; 
    GLint texSamplerLocation; // = -1;
    
    void Attach();
private:
    void checkCompileErrors(unsigned int shader, std::string type)
    {
        int success;
        char infoLog[1024];
        if (type != "PROGRAM")
        {
            glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
            if (!success)
            {
                glGetShaderInfoLog(shader, 1024, NULL, infoLog);
                std::cout << "ERROR::SHADER_COMPILATION_ERROR of type: " << type << "\n" << infoLog << "\n ----------------------------------------------------- -- " << std::endl;
            }
        }
        else
        {
            glGetProgramiv(shader, GL_LINK_STATUS, &success);
            if (!success)
            {
                glGetProgramInfoLog(shader, 1024, NULL, infoLog);
                std::cout << "ERROR::PROGRAM_LINKING_ERROR of type: " << type << "\n" << infoLog << "\n ----------------------------------------------------- -- " << std::endl;
            }
        }
    }
};
