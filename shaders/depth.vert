#version 450 core
layout (location = 0) in vec3 aPos;
layout (location = 2) in vec2 texCoords;
 
out vec2 texture_coords;

uniform mat4 lightMatrix;
uniform mat4 model;

void main() {
    texture_coords = texCoords;
    gl_Position = lightMatrix * model * vec4(aPos, 1.0);
}