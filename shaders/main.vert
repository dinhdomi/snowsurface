#version 410 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 normals;
layout (location = 2) in vec2 texCoords;
layout (location = 3) in vec3 tangent;


//layout (location = 1) in vec2 aTexCoord;

out vec3 Normal;
out vec3 Position;
out vec3 screenPosition;
out vec2 TexCoord;
out vec4 lightSpaceFragPosition;
out mat3 TBN;
out vec3 TangentFragPos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat3 normal_matrix;
uniform mat4 lightSpaceMatrix;

uniform float accumulation;

uniform sampler2D texture_shadow1;
uniform sampler2D texture_shadow2;
uniform sampler2D texture_noise;
uniform float noise_number;

uniform sampler2D texture_acc;

float shadowCalc(vec4 fragPos) {

    vec4 shadowPos = fragPos / fragPos.w;

    float bias =  0.0012;
    float visibility = 1.0f;
    if ((shadowPos.x < 0 || shadowPos.x > 1 || shadowPos.y < 0 || shadowPos.y > 1 || shadowPos.z < 0 || shadowPos.z > 1)){
        visibility = 1.0f;
    }else{
        float shadowDepth = texture(texture_shadow1, shadowPos.xy).r;
        if(shadowDepth<shadowPos.z-bias)
        visibility = 0.0f;
    }
    return visibility;
}

float rand(vec2 co){
    return fract(sin(dot(co, vec2(12.9898, 78.233))) * 43758.5453);
}

void main() {

    Position = aPos;
    mat3 normal_mat = mat3(transpose(inverse(model)));
    
    Normal = normalize( normal_mat * normals);
    
    TexCoord = texCoords;
    lightSpaceFragPosition = lightSpaceMatrix * vec4(Position, 1.0f);

    
    float shadow = shadowCalc(lightSpaceFragPosition);
    
//    if (shadow == 1.0f) {
//        Position.y += accumulation * ( texture(texture_noise, TexCoord).r + 0.01f);
//    }
    
    //mat4 normalMat = transpose(inverse(model));
    
    //Normal = normal_matrix * normals;
    //Normal = normalize( (normalMat * vec4(normals, 0.0) ).xyz);
    
    float height = texture(texture_acc, texCoords).r;

    vec3 T = normalize(normal_mat * tangent);
    vec3 N = normalize(normal_mat * normals);
    T = normalize(T - dot(T, N) * N);
    vec3 B = cross(N, T);
    
    TBN = transpose(mat3(T, B, N));
    TangentFragPos = TBN * Position;
    
    if (height >= 1.0f) height = 1.0f;
////    
//    Position +=  Normal * height;
    gl_Position = vec4(aPos, 1.0f);
    
    screenPosition = (view * model * vec4(aPos, 1.0f)).xyz;
    //TexCoord = vec2(aTexCoord.x, aTexCoord.y);
}