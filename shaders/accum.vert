#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 2) in vec2 texCoords;

out vec2 texture_coords;
out vec3 input_position;
out vec2 pos;


void main() {
    texture_coords = texCoords;
    pos = vec2(aPos.x, aPos.y);
    input_position = aPos;
    
    gl_Position = vec4(aPos, 1.0f);
    //gl_Position =  vec4(aPos.x, aPos.z, 0.0f, 1.0f);
}