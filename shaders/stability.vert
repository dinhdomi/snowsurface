#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normals;
layout (location = 2) in vec2 texture_coordinates;
layout (location = 3) in vec3 tangent;


out vec3 oPosition;
out vec3 oNormals;
out vec3 oTangent;
out vec2 oTexture_coordinates;

uniform mat3 normal_matrix;

void main() {
    
    vec2 pos = texture_coordinates * 2.0f - vec2(1.0f,1.0f);
    gl_Position = vec4(pos, 0.0f, 1.0f);
    
    oPosition = position;
    oNormals = normal_matrix * normals;
    oTexture_coordinates = texture_coordinates;
    oTangent = tangent;
}