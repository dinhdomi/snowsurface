#version 330 core

in vec2 texture_coordinates;

uniform sampler2D first;
uniform sampler2D second;

out vec4 resulting_color;

void main() {

    vec4 f = texture(first, texture_coordinates);
    vec4 s = texture(second, texture_coordinates);
    
    resulting_color = vec4(min(f.r + s.r, 1.0f), min(f.g + s.g, 1.0f), min(f.b + s.b, 1.0f), 1.0f);
}