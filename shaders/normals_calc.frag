#version 330 core

in vec2 texture_coordinates;

uniform sampler2D acc_map;

out vec4 resulting_normal;

const vec2 size = vec2(0.3,0.0);
const ivec3 off = ivec3(-1,0,1);
const ivec2 tex_off = ivec2(1, 1);

void main() {

    
    
    //aplying the sobel filter
    vec3 normal;
    
    //SOBEL
//    float Xll =-1 *textureOffset(acc_map, texture_coordinates, -tex_off.xy).x;
//    float Xlr = textureOffset(acc_map, texture_coordinates, ivec2(tex_off.x, -tex_off.y)).x;
//    float Xmr = 2 *textureOffset(acc_map, texture_coordinates, ivec2(tex_off.x, 0)).x;
//    float Xur = 1 * textureOffset(acc_map, texture_coordinates, tex_off).x;
//    float Xul = -1 * textureOffset(acc_map, texture_coordinates, ivec2(-tex_off.x, tex_off.y)).x;
//    float Xml = -2 * textureOffset(acc_map, texture_coordinates, ivec2(-tex_off.x, 0)).x;
//
//
//    float Yll = 1 * textureOffset(acc_map, texture_coordinates, -tex_off.xy).x;
//    float Ylm =  2 * textureOffset(acc_map, texture_coordinates, ivec2(0, -tex_off.y)).x;
//    float Ylr = 1 *textureOffset(acc_map, texture_coordinates, ivec2(tex_off.x, -tex_off.y)).x;
//    float Yur = - 1 * textureOffset(acc_map, texture_coordinates, tex_off).x;
//    float Yum = -2 * textureOffset(acc_map, texture_coordinates, ivec2(0, tex_off.y)).x;
//    float Yul = -1 * textureOffset(acc_map, texture_coordinates, ivec2(-tex_off.x, tex_off.y)).x;
    
//    normal.x = (Xll + Xlr + Xmr + Xur + Xul + Xmr);
//    normal.y = (Yll + Ylm + Ylr + Yur + Yum + Yul);


        float bottomleft =textureOffset(acc_map, texture_coordinates, -tex_off.xy).x;
        float bottomright = textureOffset(acc_map, texture_coordinates, ivec2(tex_off.x, -tex_off.y)).x;
        float right = textureOffset(acc_map, texture_coordinates, ivec2(tex_off.x, 0)).x;
        float topright = textureOffset(acc_map, texture_coordinates, tex_off).x;
        float topleft = textureOffset(acc_map, texture_coordinates, ivec2(-tex_off.x, tex_off.y)).x;
        float left = textureOffset(acc_map, texture_coordinates, ivec2(-tex_off.x, 0)).x;
        float top = textureOffset(acc_map, texture_coordinates, ivec2(0, tex_off.y)).x;
        float bottom = textureOffset(acc_map, texture_coordinates, ivec2(0, -tex_off.y)).x;
    
    
    //SCHARR
    normal.x = (topleft*3.0 + left*10.0 + bottomleft*3.0 - topright*3.0 - right*10.0 - bottomright*3.0) * 255.0 * -1.0f;
    normal.y = (topleft*3.0 + top*10.0 + topright*3.0 - bottomleft*3.0 - bottom*10.0 - bottomright*3.0) * 255.0 * -1.0f;
    //normal.z = 83.3888630f;
    normal.z = 360;
    
    normal = normalize(normal);
    
    normal.xy = normal.xy * 0.5f + 0.5f;
    
    resulting_normal = vec4(normal, texture(acc_map, texture_coordinates));
//
//    vec4 wave = texture(acc_map, texture_coordinates);
//    float s11 = wave.x;
//    float s01 = textureOffset(acc_map, texture_coordinates, off.xy).x;
//    float s21 = textureOffset(acc_map, texture_coordinates, off.zy).x;
//    float s10 = textureOffset(acc_map, texture_coordinates, off.yx).x;
//    float s12 = textureOffset(acc_map, texture_coordinates, off.yz).x;
//
//    //vec3 va = normalize(vec3(size.x, s21-s01, size.y));
//    //vec3 vb = normalize(vec3(size.y, s12-s10, -size.x));
//    
//    //vec3 va = normalize(vec3(size.xy,s21-s11));
//    vec3 va = normalize(vec3(size.x, s21-s01, size.y));
//    vec3 vb = normalize(vec3(size.y,s12-s10, -size.x));
//    
//    
//    
//    //vec4 bump = vec4( cross(va,vb), s11 );
//
//    resulting_normal = vec4( cross(va,vb), s11);
//    //resulting_normal = vec4(wave);
    
}