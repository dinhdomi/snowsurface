#version 330 core
layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

in vec2 texture_coords[3];
in vec2 pos[3];

uniform sampler2D occlusion1;
uniform sampler2D occlusion2;

out float accumulation;
out float texID;

out vec2 texc1;
out vec2 texc2;

out float noise;

out vec2 c1;
out vec2 c2;
out vec2 c3;


void main() {

    vec2 coords = texture_coords[0] + texture_coords[1] + texture_coords[2];
    coords = coords / 3.0;

    vec4 ids = texture(occlusion1, coords);
    vec4 map = texture(occlusion2, coords);
    
    float minx = map[0], maxx = map[2], miny = map[1], maxy = map[3];

    vec2 minPoint = vec2(minx, miny);
    vec2 maxPoint = vec2(maxx, maxy);

    minPoint = (minPoint*2) + vec2(-1.0,-1.0);
    maxPoint = (maxPoint*2) + vec2(-1.0,-1.0);

    float area = ((maxPoint.x - minPoint.x)*1024) * ((maxPoint.y - minPoint.y)*1024);
    
    for(int i = 0; i < 3; i++)
    {
        vec2 poss;
        if(pos[i].x < 0)
        {
            poss.x = minPoint.x;
            //gModifier.x = -1.0;
        } else {
            poss.x = maxPoint.x;
            //gModifier.x = 1.0;
        }

        if(pos[i].y < 0)
        {
            poss.y = minPoint.y;
            //gModifier.y = -1.0;
        } else {
            poss.y = maxPoint.y;
            //gModifier.y = 1.0;
        }


        gl_Position = vec4(poss, 0.0, 1.0);
        noise = ids[1];

//        if(area < 500)
//        {
//            noise = ids[1];
//        }
    
        texID = ids[0];
        //accumulation = 0.02f * noise;
        accumulation = 1.0f;
        EmitVertex();
    }
    
    EndPrimitive();
}
