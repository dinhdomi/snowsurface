﻿#include "Camera.h"

#include <iostream>

Camera::Camera() {

    position = glm::vec3(2,0,0);
    target = glm::vec3(0.0f, 0.0f, 0.0f);

    direction = normalize(position-target);
    right_axis = glm::normalize(glm::cross(up, direction));
    camera_up =  glm::normalize(glm::cross(direction, right_axis));
    pitch_a = 0;
    yaw_a = -90.0f;
    
    // delta = 4.0f ;
    // position = glm::vec3(0.0f,1.0f,2.0f);
    // target = glm::vec3(0.0f, 0.0f, 0.0f);
    //
    // direction = glm::vec3(0.0f, 0.0f, -1.0f);
    //direction = glm::normalize(position - target);
    
    right_axis = glm::normalize(glm::cross(up, direction));
    camera_up =  glm::normalize(glm::cross(direction, right_axis));
}

void Camera::Rotate(float pitch, float yaw) {

    // std::cout << yaw_a << std::endl;
    // std::cout << pitch_a << std::endl;
    yaw_a += yaw;
    pitch_a += pitch;
    if (pitch_a > CAMERA_ELEVATION_MAX) pitch_a = CAMERA_ELEVATION_MAX;
    if (pitch_a < -CAMERA_ELEVATION_MAX) pitch_a = -CAMERA_ELEVATION_MAX;
    direction.x = cos(glm::radians(yaw_a)) * cos(glm::radians(pitch_a));
    direction.y = sin(glm::radians(pitch_a));
    direction.z = sin(glm::radians(yaw_a)) * cos(glm::radians(pitch_a));
    direction = glm::normalize(direction);
    right_axis = glm::normalize(glm::cross(up, direction));
    camera_up =  glm::normalize(glm::cross(direction, right_axis));
}

void Camera::MoveStraight(float dir, float timedelta) {
    
    glm::vec3 future_pos =  position + dir * direction * 0.4f;

    position = future_pos;
}

void Camera::MoveSideays(float dir, float timedelta) {

    glm::vec3 future_pos = position + dir * right_axis * 0.4f;
    
    position = future_pos;
}
