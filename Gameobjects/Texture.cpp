﻿#include "Texture.h"

#define STB_IMAGE_IMPLEMENTATION  
#include <iostream>

#include "../Core/Statics.h"
#include "../include/stb_image.h"

void Texture::createTexture(std::string file_path) {

    stbi_set_flip_vertically_on_load(true);
    
    glGenTextures(1, &texture_id) ;
    glBindTexture(GL_TEXTURE_2D, texture_id);
    
    int width, height, nrChannels;
    unsigned char* image_data = stbi_load(file_path.c_str(), &width, &height, &nrChannels, 0);
    if (!image_data) {
        throw "FAILED TO LOOAD IMAGE: " + file_path;
    }

    std::cout << nrChannels << std::endl;
    //
    // for(auto i = 0; i < 1024*1024 * 4; i++) {
    //         image_data[i] = 128;
    // }

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image_data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
    //glGenerateMipmap(GL_TEXTURE_2D);

    CHECK_GL_ERROR();
    stbi_image_free(image_data);
}
