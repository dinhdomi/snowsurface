﻿#include "GameObject.h"

#include "../Core/GameState.h"

//
// GameObject::GameObject() {
//     this->geometry = new Mesh();
// }

GameObject::GameObject(std::string path, std::string diffuse_texture, std::string normal_texture, glm::vec3 position, Shader* shader) {

    this->geometry = new Mesh(path, shader, diffuse_texture, normal_texture);
    this->position = position;

    glGenTextures(1, &accumulation_texture);
    glBindTexture(GL_TEXTURE_2D, accumulation_texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, screen_height, screen_height, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

    glGenTextures(1, &normal_map_texture);
    glBindTexture(GL_TEXTURE_2D, normal_map_texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, screen_height, screen_height, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
}

void GameObject::replaceAccTexture(GLuint replacement) {

    glDeleteTextures(1, &accumulation_texture);

    accumulation_texture = replacement;
}

void GameObject::replaceNormalMap(GLuint replacement) {
    glDeleteTextures(1, &normal_map_texture);

    normal_map_texture = replacement;
    
}

Mesh& GameObject::get_geometry() {
    return *geometry;
}
