﻿#include "Grid.h"


Grid::Grid() {
}

void Grid::Init() {

    auto quads = screen_height * screen_height;

    float vert_coords[4 * 2] = {
        -1, -1,
         1, -1,
         1,  1,
        -1,  1
    };

    float *indices = new float[quads * 2 * 3];

    float *tex_coords = new float[quads * 2 * 3 * 2];
    const float offset = 1.0f / static_cast<float>(screen_height);
    
    auto i_idx = 0;
    auto t_idx = 0;
    
    for(auto x = 0; x < screen_height; x++) {
        for(auto y = 0; y < screen_height; y++) {

            glm::vec2 current_tex = glm::vec2( static_cast<float>(x) / static_cast<float>(screen_height),
                static_cast<float>(y) / static_cast<float>(screen_height) );
            
            indices[i_idx + 0] = 0;
            indices[i_idx + 1] = 1;
            indices[i_idx + 2] = 2;
            
            indices[i_idx + 3] = 0;
            indices[i_idx + 4] = 2;
            indices[i_idx + 5] = 3;

            tex_coords[t_idx + 0] = current_tex.x;
            tex_coords[t_idx + 1] = current_tex.y;

            tex_coords[t_idx + 2] = current_tex.x + offset;
            tex_coords[t_idx + 3] = current_tex.y;

            tex_coords[t_idx + 4] = current_tex.x + offset;
            tex_coords[t_idx + 5] = current_tex.y + offset;

            tex_coords[t_idx + 6] = current_tex.x;
            tex_coords[t_idx + 7] = current_tex.y;

            tex_coords[t_idx + 8] = current_tex.x + offset;
            tex_coords[t_idx + 9] = current_tex.y + offset;

            tex_coords[t_idx + 10] = current_tex.x;
            tex_coords[t_idx + 11] = current_tex.y + offset;
            
            i_idx += 6;
            t_idx += 12;
        }
    }

    vertex_array.bind();

    vertex_buffer.bind();
    glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(float) * 2 + quads * 2 * 3 * 2 * sizeof(float) , 0, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, 4 * 2 *sizeof(float), &vert_coords);
    glBufferSubData(GL_ARRAY_BUFFER, 4 * 2 *sizeof(float), quads * 2 * 3 * 2 * sizeof(float), tex_coords);
    CHECK_GL_ERROR();
    
    element_buffer.Bind();
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
    CHECK_GL_ERROR();
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
    glEnableVertexAttribArray(0);
    CHECK_GL_ERROR();
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)(4 *2* sizeof(float)));
    glEnableVertexAttribArray(1);

    CHECK_GL_ERROR();
    vertex_array.unbind();
}

void Grid::InitD(Shader* shader) {
    glm::vec2 points[4];
    glm::vec2 tex[4];
    
    points[0] = glm::vec2(-1.0,-1.0);
    points[1] = glm::vec2(1.0,-1.0);
    points[2] = glm::vec2(1.0,1);
    points[3] = glm::vec2(-1.0,1);

    tex[0] = glm::vec2(0.0,0.0);
    tex[1] = glm::vec2(1.0,0.0);
    tex[2] = glm::vec2(1.0,1.0);
    tex[3] = glm::vec2(0.0,1.0);
    
    auto quads = screen_height*screen_height;
    int* gridFaces = new int[quads*6];
    float* gridPoints = new float[quads*4*2];
    int* gridTexel = new int[quads*4*2];
    float* gridTex = new float[quads*4*2];

    int arrayindex = 0;
    int pointindex = 0;
    int faceindex = 0;
    
    for(int y = 0; y < screen_height; y++)
    {
        for(int x = 0; x < screen_height; x++)
        {

            gridFaces[faceindex] = pointindex;
            gridFaces[faceindex+1] = pointindex+1;
            gridFaces[faceindex+2] = pointindex+2;

            gridFaces[faceindex+3] = pointindex;
            gridFaces[faceindex+4] = pointindex+2;
            gridFaces[faceindex+5] = pointindex+3;

            faceindex+=6;

            
            glm::vec2 textureCoords = glm::vec2((float)x/(float)screen_height, (float)y/(float)screen_height);

            for(int i = 0; i < 4; i++)
            {
                gridTex[arrayindex] = textureCoords.x;
                gridTex[arrayindex+1] = textureCoords.y;

                //gridPoints[arrayindex] = (gridTex[arrayindex] * 2.0)-1.0;
                //gridPoints[arrayindex+1] = (gridTex[arrayindex+1] * 2.0)-1.0;

                gridPoints[arrayindex] = points[i].x;
                gridPoints[arrayindex+1] = points[i].y;

                //cout << gridPoints[arrayindex] << " . " << gridPoints[arrayindex+1] << endl;

                gridTexel[arrayindex] = x;
                gridTexel[arrayindex+1] = y;

				

                arrayindex+=2;
                pointindex++;
            }
        }
    }

    

    
    vertex_array.bind();
    
    GLuint *glGridBuffer = new GLuint[4];
    glGenBuffers(4, glGridBuffer);

    glBindBuffer(GL_ARRAY_BUFFER, glGridBuffer[0]);
    glBufferData(GL_ARRAY_BUFFER, quads*4*2*sizeof(GLfloat), gridPoints, GL_STATIC_DRAW);

    
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0); 
    glEnableVertexAttribArray(0);


    glBindBuffer(GL_ARRAY_BUFFER, glGridBuffer[2]);
    glBufferData(GL_ARRAY_BUFFER, quads*4*2*sizeof(GLfloat), gridTex, GL_STATIC_DRAW);

    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0); 
    glEnableVertexAttribArray(2);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, glGridBuffer[3]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, quads*6*sizeof(GLint), gridFaces, GL_STATIC_DRAW);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glEnableVertexAttribArray(0);
    glBindVertexArray(0);
    glUseProgram(0);
}

void Grid::BindVAO() {
    vertex_array.bind();
}

void Grid::UnbindVAO() {
    vertex_array.unbind();
}
