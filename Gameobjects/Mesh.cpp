﻿#include "Mesh.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

#include "../Core/Statics.h"
//


Mesh::Mesh(std::string file_path, Shader* shader_program, std::string diffuse_texture_path, std::string normal_texture) {
  shader = shader_program;
  Assimp::Importer importer;

  // Unitize object in size (scale the model to fit into (-1..1)^3)
  importer.SetPropertyInteger(AI_CONFIG_PP_PTV_NORMALIZE, 1);

  // Load asset from the file - you can play with various processing steps
  const aiScene * scn = importer.ReadFile(file_path.c_str(), 0
      | aiProcess_Triangulate             // Triangulate polygons (if any).
      | aiProcess_PreTransformVertices    // Transforms scene hierarchy into one root with geometry-leafs only. For more see Doc.
      | aiProcess_GenSmoothNormals        // Calculate normals per vertex.
      | aiProcess_JoinIdenticalVertices
      | aiProcess_CalcTangentSpace);


  // abort if the loader fails
  if(scn == NULL) {
    std::cerr << "assimp error: " << importer.GetErrorString() << std::endl;
    throw "Unable to load assimp";
  }

  if(scn->mNumMeshes != 1) {
    std::cerr << "this simplified loader can only process files with only one mesh" << std::endl;
    throw "Unable to load assimp";
  }

  // in this phase we know we have one mesh in our loaded scene, we can directly copy its data to OpenGL ...
  const aiMesh * mesh = scn->mMeshes[0];
  // vertex buffer object, store all vertex positions and normals
  //glGenBuffers(1, &((*geometry)->vertexBufferObject));
  //glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
  
  vertex_buffer.bind();
  
  glBufferData(GL_ARRAY_BUFFER, 11*sizeof(float)*mesh->mNumVertices, 0, GL_STATIC_DRAW); // allocate memory for vertices, normals, and texture coordinates
  // first store all vertices
  glBufferSubData(GL_ARRAY_BUFFER, 0, 3*sizeof(float)*mesh->mNumVertices, mesh->mVertices);
  // then store all normals
  glBufferSubData(GL_ARRAY_BUFFER, 3*sizeof(float)*mesh->mNumVertices, 3*sizeof(float)*mesh->mNumVertices, mesh->mNormals);
  
  // just texture 0 for now
  float *textureCoords = new float[2 * mesh->mNumVertices];  // 2 floats per vertex
  float *currentTextureCoord = textureCoords;

  // copy texture coordinates
  aiVector3D vect;
  aiVector3D tan;

  float *tangentCoord = new float[3 * mesh->mNumVertices];
  
  if(mesh->HasTextureCoords(0) ) {
    // we use 2D textures with 2 coordinates and ignore the third coordinate
    for(unsigned int idx=0; idx<mesh->mNumVertices; idx++) {
      vect = (mesh->mTextureCoords[0])[idx];
      tan = mesh->mTangents[idx];
      
      *currentTextureCoord++ = vect.x;
      *currentTextureCoord++ = vect.y;

      tangentCoord[3*idx] = tan.x;
      tangentCoord[3*idx+1] = tan.y;
      tangentCoord[3*idx+2] = tan.z;

    }
  }
  
  
  // finally store all texture coordinates
  glBufferSubData(GL_ARRAY_BUFFER, 6*sizeof(float)*mesh->mNumVertices, 2*sizeof(float)*mesh->mNumVertices, textureCoords);

  if (mesh->HasTangentsAndBitangents())
    glBufferSubData(GL_ARRAY_BUFFER, 8*sizeof(float)*mesh->mNumVertices, 3*sizeof(float)*mesh->mNumVertices, tangentCoord);
  
  std::cout << "SUBBUFFER DATA INITIATED FOR " << file_path.c_str() << std::endl;
  // copy all mesh faces into one big array (assimp supports faces with ordinary number of vertices, we use only 3 -> triangles)
  unsigned int *indices = new unsigned int[mesh->mNumFaces * 3];
  for(unsigned int f = 0; f < mesh->mNumFaces; ++f) {
    indices[f*3 + 0] = mesh->mFaces[f].mIndices[0];
    indices[f*3 + 1] = mesh->mFaces[f].mIndices[1];
    indices[f*3 + 2] = mesh->mFaces[f].mIndices[2];
  }

  
  // copy our temporary index array to OpenGL and free the array
  // glGenBuffers(1, &((*geometry)->elementBufferObject));
  // glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (*geometry)->elementBufferObject);
  element_buffer.Bind();
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * sizeof(unsigned) * mesh->mNumFaces, indices, GL_STATIC_DRAW);

  delete [] indices;

  // copy the material info to MeshGeometry structure
  const aiMaterial *mat  = scn->mMaterials[mesh->mMaterialIndex];
  aiColor4D color;
  aiString name;
  aiReturn retValue = AI_SUCCESS;

  CHECK_GL_ERROR();
  // Get returns: aiReturn_SUCCESS 0 | aiReturn_FAILURE -1 | aiReturn_OUTOFMEMORY -3
  mat->Get(AI_MATKEY_NAME, name); // may be "" after the input mesh processing. Must be aiString type!

  if((retValue = aiGetMaterialColor(mat, AI_MATKEY_COLOR_DIFFUSE, &color)) != AI_SUCCESS)
    color = aiColor4D(0.0f, 0.0f, 0.0f, 0.0f);

  material.diffuse = glm::vec3(color.r, color.g, color.b);

  if ((retValue = aiGetMaterialColor(mat, AI_MATKEY_COLOR_AMBIENT, &color)) != AI_SUCCESS)
    color = aiColor4D(0.0f, 0.0f, 0.0f, 0.0f);
  material.ambient = glm::vec3(color.r, color.g, color.b);

  if ((retValue = aiGetMaterialColor(mat, AI_MATKEY_COLOR_SPECULAR, &color)) != AI_SUCCESS)
    color = aiColor4D(0.0f, 0.0f, 0.0f, 0.0f);
  material.specular = glm::vec3(color.r, color.g, color.b);

  ai_real shininess, strength;
  unsigned int max;	// changed: to unsigned

  max = 1;	
  if ((retValue = aiGetMaterialFloatArray(mat, AI_MATKEY_SHININESS, &shininess, &max)) != AI_SUCCESS)
    shininess = 1.0f;
  max = 1;
  if((retValue = aiGetMaterialFloatArray(mat, AI_MATKEY_SHININESS_STRENGTH, &strength, &max)) != AI_SUCCESS)
    strength = 1.0f;
  material.shininess = shininess * strength;

  texture.texture_id = 0;

  
  // load texture image
  if (mat->GetTextureCount(aiTextureType_DIFFUSE) > 0) {
    // get texture name 
    aiString path; // filename

    aiReturn texFound = mat->GetTexture(aiTextureType_DIFFUSE, 0, &path);
    std::string textureName = path.data;

    size_t found = file_path.find_last_of("/\\");
    // insert correct texture file path 
    if(found != std::string::npos) { // not found
      //subMesh_p->textureName.insert(0, "/");
      textureName.insert(0, file_path.substr(0, found+1));
    }

    std::cout << "Loading texture file: " << textureName << std::endl;
    texture.createTexture(diffuse_texture_path);
  }
  CHECK_GL_ERROR();

  if (mat->GetTextureCount(aiTextureType_HEIGHT) > 0)
  {
    aiString path; // filename

    aiReturn texFound = mat->GetTexture(aiTextureType_HEIGHT, 0, &path);
    std::string textureName = path.data;

    size_t found = file_path.find_last_of("/\\");
    // insert correct texture file path 
    if(found != std::string::npos) { // not found
      //subMesh_p->textureName.insert(0, "/");
      textureName.insert(0, file_path.substr(0, found+1));
    }

    std::cout << "Loading texture file: " << textureName << std::endl;
    normal_map.createTexture(normal_texture);
  }
  
  // glGenVertexArrays(1, &((*geometry)->vertexArrayObject));
  // glBindVertexArray((*geometry)->vertexArrayObject);
  vertex_array.bind();

  // glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (*geometry)->elementBufferObject); // bind our element array buffer (indices) to vao
  // glBindBuffer(GL_ARRAY_BUFFER, (*geometry)->vertexBufferObject);
  element_buffer.Bind();
  vertex_buffer.bind();
  CHECK_GL_ERROR();
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)(3 * sizeof(float) * mesh->mNumVertices));

  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void*)(6 * sizeof(float) * mesh->mNumVertices));
  CHECK_GL_ERROR();
  
  if (mesh->HasTangentsAndBitangents())
  {
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void*)(8 * sizeof(float) * mesh->mNumVertices));
    
  }
  glBindVertexArray(0);

  std::cout << "NUmber of faces: " << mesh->mNumFaces << std::endl;
  triangles = mesh->mNumFaces;
  
}

VAO Mesh::get_vao() {
    return this->vertex_array;
}

unsigned Mesh::get_num_vertices() {
    return this->vertices;
}
