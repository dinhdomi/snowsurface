﻿#pragma once

#include <string>
#include "Material.h"
#include "Texture.h"
#include "../Core/Buffers/VAO.h"
#include "../Core/Buffers/VBO.h"
#include "../Core/Buffers/EBO.h"

#include "../shaders/Shader.h"

class Mesh {
public:
    // Mesh();
    Mesh(std::string file_path, Shader* shader_program, std::string texture_diff_path, std::string normal_texture);
    

    VAO get_vao();
    unsigned int get_num_vertices();
public:
    unsigned int vertices;
    unsigned int triangles;
    
    VAO vertex_array;
    VBO vertex_buffer;
    EBO element_buffer;
    Material material;
    Shader* shader;
    
    Texture texture;
    Texture normal_map;
};

