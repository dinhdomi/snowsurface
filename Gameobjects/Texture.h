﻿#pragma once

#include <string>
#include <glad/glad.h>

class Texture {
public:
    GLuint texture_id;
    void createTexture(std::string file_path);
};
