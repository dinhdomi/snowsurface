﻿#pragma once

#include <glm/glm.hpp>
#define CAMERA_ELEVATION_MAX 45.0f

class Camera {
public:
    Camera();
    glm::vec3 position;
    glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
    glm::vec3 camera_up;
    glm::vec3 direction;
    glm::vec3 target;
    glm::vec3 right_axis;

    
    float pitch_a;
    float yaw_a;


    void Rotate(float pitch, float yaw);
    void MoveStraight(float dir, float timedelta);
    void MoveSideays(float dir, float timedelta);
};
