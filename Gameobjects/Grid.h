﻿#pragma once
#include "Material.h"
#include "Texture.h"
#include "../Core/Buffers/VAO.h"
#include "../Core/Buffers/VBO.h"
#include "../Core/Buffers/EBO.h"
#include "../Core/GameState.h"
#include "../shaders/Shader.h"

class Grid {
public:
    Grid();
    void Init();
    void InitD(Shader *shader);
    void BindVAO();
    void UnbindVAO();
private:
    VAO vertex_array;
    VBO vertex_buffer;
    EBO element_buffer;
    
    Shader* shader;
    
    Texture texture;
    Texture normal_map;
};
