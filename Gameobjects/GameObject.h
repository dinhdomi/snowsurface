﻿#pragma once
#include <string>
#include <glm/glm.hpp>

#include "Mesh.h"

class GameObject
{
public:
    // GameObject();
    GameObject(std::string path, std::string diffuse_texture, std::string normal_texture, glm::vec3 position, Shader* shader);
    glm::vec3 position = glm::vec3(0,0,0);
    glm::vec3 scale = glm::vec3(1,1,1);

    GLuint getAccTexture() const { return accumulation_texture;}
    GLuint getNormalTexture() const { return normal_map_texture;}
    void replaceAccTexture(GLuint replacement);
    void replaceNormalMap(GLuint replacement);
    float deltaTime;
    float size;

    
    Mesh& get_geometry();

private:
    Mesh* geometry;
    GLuint accumulation_texture;
    GLuint normal_map_texture;
};
