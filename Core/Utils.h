﻿#pragma once
#include <glad/glad.h>

#include "../shaders/Shader.h"
#include "Buffers/FBO.h"
#include "Buffers/VAO.h"
#include "Buffers/VBO.h"

class Utils {
public:

    static void Init();
    static void GaussianBlur(GLuint *texture,  GLuint *output);
    static GLuint Increment(GLuint new_tex, GLuint current);
    static GLuint CalculateNormal(GLuint acc_map);
    static void Dilate(GLuint acc_map, GLuint target);


private:
    static VAO* vertex_array;
    static VBO* positions_array;
    static VBO* texcoord_array;

    static FBO* blur_fbo;
    
    static GLuint temp_texture;

    static Shader* vertical_blur_shader_;
    static Shader* horizontal_blur_shader_;
    static Shader* increment_shader_;
    static Shader* normal_calc_shader_;
    static Shader* dilate_shader;
    static void InitTempTexture();
};
