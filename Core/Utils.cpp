﻿#include "Utils.h"

#include <glm/vec2.hpp>

#include "GameState.h"
#include "Buffers/FBO.h"

VAO* Utils::vertex_array;
VBO* Utils::positions_array;
VBO* Utils::texcoord_array;
FBO* Utils::blur_fbo;

GLuint Utils::temp_texture;
Shader* Utils::vertical_blur_shader_;
Shader* Utils::horizontal_blur_shader_;
Shader* Utils::increment_shader_;
Shader* Utils::normal_calc_shader_;
Shader* Utils::dilate_shader;

void Utils::InitTempTexture() {
    glGenTextures(1, &temp_texture);
    glBindTexture(GL_TEXTURE_2D, temp_texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, screen_height, screen_height, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

}
void Utils::Init() {

    vertex_array = new VAO;
    positions_array = new VBO;
    texcoord_array = new VBO;
    blur_fbo = new FBO;
    
    vertex_array->bind();

    GLfloat quad[] = { -1.f,-1.f, 1.f, -1.f, 1.f,1.f,
                      -1.f,-1.f,  1.f,1.f,  -1.f, 1.f };

    GLfloat tex[] = { 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
                     0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f
    };

    positions_array->bind();
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0); 
    glEnableVertexAttribArray(0);
    glBufferData(GL_ARRAY_BUFFER, 6 * 2 * sizeof(float), quad, GL_STATIC_DRAW);
    
    
    texcoord_array->bind();
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0); 
    glEnableVertexAttribArray(1);
    glBufferData(GL_ARRAY_BUFFER, 6 * 2 * sizeof(float), tex, GL_STATIC_DRAW);
   

    vertex_array->unbind();
    
    float borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);


    InitTempTexture();
    
    vertical_blur_shader_ = new Shader("./shaders/VerticalBlur.vert", "./shaders/VerticalBlur.frag");
    horizontal_blur_shader_ = new Shader("./shaders/HorizontalBlur.vert", "./shaders/HorizontalBlur.frag");
    increment_shader_ = new Shader("./shaders/Increment.vert", "./shaders/Increment.frag");
    normal_calc_shader_ = new Shader("./shaders/normals_calc.vert", "./shaders/normals_calc.frag");
    dilate_shader = new Shader("./shaders/dilate.vert", "./shaders/dilate.frag");
    
    CHECK_GL_ERROR();
    
}

GLuint Utils::Increment(GLuint new_tex, GLuint current) {

    const GLenum buffers[]{GL_COLOR_ATTACHMENT0};
    glUseProgram(increment_shader_->program_id);

    GLuint result;
    glGenTextures(1, &result);
    glBindTexture(GL_TEXTURE_2D, result);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, screen_height, screen_height, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    
    blur_fbo->Bind();
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, new_tex);
    glUniform1i(glGetUniformLocation(increment_shader_->program_id, "first"), 0);
    CHECK_GL_ERROR();

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, current);
    glUniform1i(glGetUniformLocation(increment_shader_->program_id, "second"), 1);
    CHECK_GL_ERROR();
    
    glViewport(0,0, screen_height, screen_height);
    glDrawBuffers(1, buffers);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, result, 0);
    CHECK_GL_ERROR();
    vertex_array->bind();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDrawArrays(GL_TRIANGLES, 0 ,6);

    CHECK_GL_ERROR();
    vertex_array->unbind();

    glUseProgram(0);
    blur_fbo->Unbind();
    return result;
}

GLuint Utils::CalculateNormal(GLuint acc_map) {

    GLuint result;
    glGenTextures(1, &result);
    glBindTexture(GL_TEXTURE_2D, result);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, screen_height, screen_height, 0, GL_RGBA, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    
    const GLenum buffers[]{GL_COLOR_ATTACHMENT0};
    glUseProgram(normal_calc_shader_->program_id);

    blur_fbo->Bind();
    glDrawBuffers(1, buffers);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, result, 0);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, acc_map);
    glUniform1i(glGetUniformLocation(normal_calc_shader_->program_id, "acc_map"), 0);
    CHECK_GL_ERROR();
    
    
    
   
    CHECK_GL_ERROR();
    vertex_array->bind();
    
    glViewport(0,0, screen_height, screen_height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDrawArrays(GL_TRIANGLES, 0 ,6);

    CHECK_GL_ERROR();
    vertex_array->unbind();

    glUseProgram(0);
    blur_fbo->Unbind();
    
    return result;
}

void Utils::Dilate(GLuint acc_map, GLuint target) {
    
    const GLenum buffers[]{GL_COLOR_ATTACHMENT0};
    glUseProgram(dilate_shader->program_id);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, acc_map);
    glUniform1i(glGetUniformLocation(dilate_shader->program_id, "accumulation"), 0);
    CHECK_GL_ERROR();
    blur_fbo->Bind();

    glViewport(0,0, screen_height, screen_height);
    glDrawBuffers(1, buffers);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, target, 0);
    CHECK_GL_ERROR();
    vertex_array->bind();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDrawArrays(GL_TRIANGLES, 0 ,6);

    CHECK_GL_ERROR();
    vertex_array->unbind();

    glUseProgram(0);
}

void Utils::GaussianBlur(GLuint *input, GLuint *output) {

    const GLenum buffers[]{GL_COLOR_ATTACHMENT0};
    glUseProgram(vertical_blur_shader_->program_id);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, *input);
    glUniform1i(glGetUniformLocation(vertical_blur_shader_->program_id, "pre_texture"), 0);
    CHECK_GL_ERROR();
    blur_fbo->Bind();

    glViewport(0,0, screen_height, screen_height);
    glDrawBuffers(1, buffers);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, temp_texture, 0);
    CHECK_GL_ERROR();
    vertex_array->bind();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDrawArrays(GL_TRIANGLES, 0 ,6);

    CHECK_GL_ERROR();
    vertex_array->unbind();

    glUseProgram(0);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, 0, 0);
    glUseProgram(horizontal_blur_shader_->program_id);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, temp_texture);
    glUniform1i(glGetUniformLocation(horizontal_blur_shader_->program_id, "pre_texture"), 0);
    CHECK_GL_ERROR();
    
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, *output, 0);
    CHECK_GL_ERROR();
    vertex_array->bind();
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    
    CHECK_GL_ERROR();
    vertex_array->unbind();
    
    blur_fbo->Unbind();
    glUseProgram(0);

    // glDeleteTextures(1, &temp_texture);
    // InitTempTexture();
    
}
