﻿#pragma once
#include <vector>

class GameState {
public:
    enum KEYS {
        MOVE_FORWARD,
        MOVE_LEFT,
        MOVE_BACK,
        MOVE_RIGHT,
        DEFAULT
    };
    
    
};

static std::vector<bool> KEY_PRESSED = std::vector<bool>(4, false);

static std::vector<unsigned int> accumulation_maps(8, 0);
static double prev_cursor_x = 0;
static double prev_cursor_y = 0;

static int screen_width = 100;
static int screen_height = 1024;
