﻿#pragma once
#include <glad/glad.h>

class VBO
{
public:
    VBO();
    void bind();
    void unbind();

private:
    GLuint id;
};
