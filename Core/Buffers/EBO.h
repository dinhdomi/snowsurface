﻿#pragma once
#include <glad/glad.h>

class EBO {
public:
    EBO();
    void Bind();
    void Unbind();
private:
    GLuint id;
};
