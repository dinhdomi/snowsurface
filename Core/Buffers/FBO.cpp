﻿#include "FBO.h"

FBO::FBO() {
    glGenFramebuffers(1, &id);
}

void FBO::Bind() {

    glBindFramebuffer(GL_FRAMEBUFFER, id);
}

void FBO::Unbind() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
