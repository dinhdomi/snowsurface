﻿#include "VBO.h"

VBO::VBO()
{
    glGenBuffers(1, &id);
}

void VBO::bind()
{
    glBindBuffer(GL_ARRAY_BUFFER, id);
}

void VBO::unbind()
{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}
