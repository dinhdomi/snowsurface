﻿#include "VAO.h"

VAO::VAO() {
    glGenVertexArrays(1, &id);
}

void VAO::bind() {
    glBindVertexArray(id);
}

void VAO::unbind() {
    glBindVertexArray(0);
}
