﻿#pragma once
#include <glad/glad.h>

class FBO {

public:
    FBO();
    void Bind();
    void Unbind();
private:
    GLuint id;
};
