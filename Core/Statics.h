﻿#pragma once

#include <iostream>
#include <glad/glad.h>


static void checkGLError(const char *where, int line) {
    GLenum err = glGetError();
    if(err == GL_NONE)
        return;

    std::string errString = "<unknown>";
    switch(err) {
    case GL_INVALID_ENUM:
        errString = "GL_INVALID_ENUM";
        break;
    case GL_INVALID_VALUE:
        errString = "GL_INVALID_VALUE";
        break;
    case GL_INVALID_OPERATION:
        errString = "GL_INVALID_OPERATION";
        break;
    case GL_INVALID_FRAMEBUFFER_OPERATION:
        errString = "GL_INVALID_FRAMEBUFFER_OPERATION";
        break;
    case GL_OUT_OF_MEMORY:
        errString = "GL_OUT_OF_MEMORY";
        break;
    default:;
    }
    if(where == 0 || *where == 0)
        std::cerr << "GL error occurred: " << errString << std::endl;
    else
        std::cerr << "GL error occurred in " << where << ":" << line << ": " << errString << std::endl;
}

#define CHECK_GL_ERROR() do { checkGLError(__FUNCTION__, __LINE__); } while(0)
