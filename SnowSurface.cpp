#include <GLFW/glfw3.h>
#include <glad/glad.h>
#include <cstdlib>
#include <filesystem>

#include <iostream>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Gameobjects/GameObject.h"


// #include "shaders/Shader.h"
// #include "Gameobjects/Mesh.h"


#include <windows.h>

#include "Core/GameState.h"
#include "Core/Statics.h"
//#include "Core/TextureManager.h"
#include "Core/Utils.h"
#include "Core/Buffers/FBO.h"
#include "Gameobjects/Camera.h"
#include "Gameobjects/Grid.h"

#include "test/cube.h"

Grid* screen_grid;
 Camera main_camera;
 std::vector<GameObject> gameObjects;
FBO* test_depth_buffer1;
FBO* test_depth_buffer2;
FBO* stability_fbo;

 Shader* main_shader;
Shader* shadow_shader;
Shader* debug_quad_shader;
Shader* accum_shader;
Shader* stability_shader;

GameObject* occlusion_helper;

unsigned int depth_buffer_temp;
unsigned int depthMap1;
unsigned int depthMap2;

std::vector<unsigned int> temp_acc_map(8, 0);
unsigned int accMap1;

glm::mat4 lightSpaceMatrix;
float snow_accumulation = 0;
Texture noiseTexture;

 void framebuffer_size_callback(GLFWwindow* window, int width, int height)
 {
     screen_width = width;
     screen_height = height;
     
     glViewport(0, 0, width, height);
 }

void drawTestCube(VAO vao, glm::mat4 viewMatrix, glm::mat4 projectionMatrix) {

     glUseProgram(main_shader->program_id);
     CHECK_GL_ERROR();

     glm::mat4 model = glm::translate(glm::mat4(1.0f), glm::vec3( 2.0f,  5.0f, -15.0f));
     model=glm::scale(model, glm::vec3(.4f, .4f, .4f));

     //glm::mat3 normal_matrix = glm::transpose(glm::inverse(modelRotationMatrix));
     
     glUniformMatrix4fv(glGetUniformLocation(main_shader->program_id, "projection"), 1, GL_FALSE, glm::value_ptr(projectionMatrix));
     glUniformMatrix4fv(glGetUniformLocation(main_shader->program_id, "view"), 1, GL_FALSE, glm::value_ptr(viewMatrix));
     glUniformMatrix4fv(glGetUniformLocation(main_shader->program_id, "model"), 1, GL_FALSE, glm::value_ptr(model));
     //glUniformMatrix4fv(glGetUniformLocation(shader->program_id, "normalMatrix"), 1, GL_FALSE, glm::value_ptr(normal_matrix));
     
     CHECK_GL_ERROR();

     glDrawArrays(GL_TRIANGLES, 0, 36);
     
 }
void initAccFBOs() {

     
     float borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
     glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
     CHECK_GL_ERROR();
     std::cout << "invalid FBO for ACC" << std::endl;
     // attach depth texture as FBO's depth buffer
     test_depth_buffer2->Bind();

     const GLenum buffers[] {
         GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4,
         GL_COLOR_ATTACHMENT5, GL_COLOR_ATTACHMENT6, GL_COLOR_ATTACHMENT7
     };

     glDrawBuffers(8, buffers);
     
     for(int i = 0; i < 8; i++) {
     
         glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0+i, GL_TEXTURE_2D, temp_acc_map.at(i), 0);
     }
     
     // glDrawBuffer(GL_NONE);
     // glReadBuffer(GL_NONE);
     
     test_depth_buffer2->Unbind();
     CHECK_GL_ERROR();
     
 }
void initStabFBO() {
     
     stability_fbo->Bind();
     float borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
     glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
     CHECK_GL_ERROR();
     std::cout << "invalid FBO for ACC" << std::endl;
     // attach depth texture as FBO's depth buffer
     

     const GLenum buffers[] {
         GL_COLOR_ATTACHMENT0
     };

     glDrawBuffers(1, buffers);
     
     // glDrawBuffer(GL_NONE);
     // glReadBuffer(GL_NONE);
     
     stability_fbo->Unbind();
     CHECK_GL_ERROR();
 }
VAO initTestCube() {

     VAO vao;
     VBO vbo;
     vao.bind();

     vbo.bind();
     glBufferData(GL_ARRAY_BUFFER, sizeof(cube_vertices), cube_vertices, GL_STATIC_DRAW);

     glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
     glEnableVertexAttribArray(0);
     glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
     glEnableVertexAttribArray(1);
     glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
     glEnableVertexAttribArray(2);

     
     return vao;
 }

void drawGameobjectForShadow(GameObject go, Shader& shader, int object_index) {

     glUseProgram(shader.program_id);

     glm::mat4 model = glm::translate(glm::mat4(1.0f), go.position);
     model=glm::scale(model, go.scale);

     glUniform1f(glGetUniformLocation(shader.program_id, "object_index"), (float) object_index);
     glUniformMatrix4fv(glGetUniformLocation(shader.program_id, "model"), 1, GL_FALSE, glm::value_ptr(model));

     glUniform1f(glGetUniformLocation(shader.program_id, "time_bias"), (float) ((double) rand() / (RAND_MAX)));
     
     glActiveTexture(GL_TEXTURE0);
     glBindTexture(GL_TEXTURE_2D, noiseTexture.texture_id);
     glUniform1i(glGetUniformLocation(shader.program_id, "noise_texture"), 0);
     
     Mesh& geo = go.get_geometry();
     geo.get_vao().bind();

     glViewport(0, 0, 1024, 1024);
     
     glDrawElements(GL_TRIANGLES, 3 * geo.triangles, GL_UNSIGNED_INT, 0);

     glViewport(0, 0, screen_height, screen_height);
     
     CHECK_GL_ERROR();
     geo.get_vao().unbind();
     glUseProgram(0);
 }

 void drawGameobject(GameObject &go, Shader& shader, glm::mat4 viewMatrix, glm::mat4 projectionMatrix, int index) {
     
     glUseProgram(shader.program_id);
     CHECK_GL_ERROR();
     
     glm::mat4 model = glm::translate(glm::mat4(1.0f), go.position);
     model=glm::scale(model, go.scale);
     
     const glm::mat3 modelRotationMatrix = glm::mat3(
     model[0],
     model[1],
     model[2]
   );
     
     glm::mat3 normal_matrix = glm::transpose(glm::inverse(modelRotationMatrix));
     
     glUniformMatrix4fv(glGetUniformLocation(shader.program_id, "projection"), 1, GL_FALSE, glm::value_ptr(projectionMatrix));
     glUniformMatrix4fv(glGetUniformLocation(shader.program_id, "view"), 1, GL_FALSE, glm::value_ptr(viewMatrix));
     glUniformMatrix4fv(glGetUniformLocation(shader.program_id, "model"), 1, GL_FALSE, glm::value_ptr(model));
     glUniformMatrix4fv(glGetUniformLocation(shader.program_id, "normalMatrix"), 1, GL_FALSE, glm::value_ptr(normal_matrix));
     glm::mat4 shadowBias = glm::mat4(0.5, 0.0, 0.0, 0.0,
                                 0.0, 0.5, 0.0, 0.0,
                                 0.0, 0.0, 0.5, 0.0,
                                 0.5, 0.5, 0.5, 1.0);
     
     glUniformMatrix4fv(glGetUniformLocation(shader.program_id, "lightSpaceMatrix"), 1, GL_FALSE, glm::value_ptr(shadowBias*lightSpaceMatrix));

     glUniform1f(glGetUniformLocation(shader.program_id, "accumulation"), snow_accumulation);
     
     CHECK_GL_ERROR();
     
     Mesh& geo = go.get_geometry();
     geo.get_vao().bind();

     glActiveTexture(GL_TEXTURE0);
     glBindTexture(GL_TEXTURE_2D, geo.texture.texture_id);
     glUniform1i(glGetUniformLocation(shader.program_id, "texture_diff"), 0);

     glActiveTexture(GL_TEXTURE1);
     glBindTexture(GL_TEXTURE_2D, depth_buffer_temp);
     glUniform1i(glGetUniformLocation(shader.program_id, "texture_shadow1"), 1);

     glActiveTexture(GL_TEXTURE2);
     glBindTexture(GL_TEXTURE_2D, depthMap1);
     glUniform1i(glGetUniformLocation(shader.program_id, "texture_shadow2"), 2);
     
     glActiveTexture(GL_TEXTURE3);
     glBindTexture(GL_TEXTURE_2D, noiseTexture.texture_id);
     glUniform1i(glGetUniformLocation(shader.program_id, "texture_noise"), 3);

     glActiveTexture(GL_TEXTURE4);
     glBindTexture(GL_TEXTURE_2D, go.getAccTexture());
     glUniform1i(glGetUniformLocation(shader.program_id, "texture_acc"), 4);

     glActiveTexture(GL_TEXTURE5);
     glBindTexture(GL_TEXTURE_2D, go.getNormalTexture());
     glUniform1i(glGetUniformLocation(shader.program_id, "texture_normal"), 5);

     float r3 = 0.6f + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(1.0f-0.6f)));
     glUniform1f(glGetUniformLocation(shader.program_id, "noise_number"), r3);


     glPatchParameteri(GL_PATCH_VERTICES, 3);
     //glDrawElements(GL_TRIANGLES, 3 * geo.triangles, GL_UNSIGNED_INT, 0);
     glDrawElements(GL_PATCHES, 3 * geo.triangles, GL_UNSIGNED_INT, 0);
     
     CHECK_GL_ERROR();
     geo.get_vao().unbind();
     glUseProgram(0);
 }

void initShadowmap() { 
     
     //FBO depthFBO;
     // create depth texture


     glGenTextures(1, &depth_buffer_temp);
     glBindTexture(GL_TEXTURE_2D, depth_buffer_temp);
     glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, 1024, 1024, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
     CHECK_GL_ERROR();

     glGenTextures(1, &depthMap1);
     glBindTexture(GL_TEXTURE_2D, depthMap1);
     glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, 1024, 1024, 0, GL_RGBA, GL_FLOAT, NULL);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
     CHECK_GL_ERROR();

    //create second texture
     glGenTextures(1, &depthMap2);
     glBindTexture(GL_TEXTURE_2D, depthMap2);
     glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, 1024, 1024, 0, GL_RGBA, GL_FLOAT, NULL);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
     CHECK_GL_ERROR();

     float borderColor[] = { 1.0, 1.0, 1.0, 1.0 };
     glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
     // attach depth texture as FBO's depth buffer
     test_depth_buffer1->Bind();

     const GLenum buffers[]{GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
     CHECK_GL_ERROR();

     glDrawBuffers(2, buffers);
     
     glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, depthMap1, 0);
     glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, depthMap2, 0);
     glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth_buffer_temp, 0);
          
     // glDrawBuffer(GL_NONE);
     // glReadBuffer(GL_NONE);
     
     test_depth_buffer1->Unbind();
     CHECK_GL_ERROR();
 }

void accumulation_pass(Shader accumulation) {
     glUseProgram(accumulation.program_id);

      // Mesh& geo = occlusion_helper->get_geometry();
      //  geo.get_vao().bind();

     screen_grid->BindVAO();
     glActiveTexture(GL_TEXTURE0);
     glBindTexture(GL_TEXTURE_2D, depthMap1);
     glUniform1i(glGetUniformLocation(accumulation.program_id, "occlusion1"), 0);
     
     glActiveTexture(GL_TEXTURE1);
     glBindTexture(GL_TEXTURE_2D, depthMap2);
     glUniform1i(glGetUniformLocation(accumulation.program_id, "occlusion2"), 1);
     
     glViewport(0, 0, screen_height, screen_height);
     test_depth_buffer2->Bind();

     glDisable(GL_CULL_FACE);
     glDisable(GL_DEPTH_TEST);
     glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
     glDrawElements(GL_TRIANGLES, screen_height*screen_height * 2 * 3, GL_UNSIGNED_INT, 0);
     glEnable(GL_DEPTH_TEST);
     glEnable(GL_CULL_FACE);
     
     CHECK_GL_ERROR();
     
     // geo.get_vao().unbind();
     
       glViewport(0, 0, screen_width, screen_height);
     // // //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
       test_depth_buffer2->Unbind();

     screen_grid->UnbindVAO();
     glBindVertexArray(0);
 }

void shadowPass(Shader depth_shader) {

     glUseProgram(depth_shader.program_id);
     
     double x_off = ((double) rand() / (RAND_MAX));
     double y_off = ((double) rand() / (RAND_MAX));

     ((double) rand() / (RAND_MAX));
     
     double x_off_dir = ((double) rand() / (RAND_MAX));
     double y_off_dir = ((double) rand() / (RAND_MAX));
     
     x_off = (x_off * 2.0f - 1.0f) * 0.18f;
     y_off = (y_off * 2.0f - 1.0f) * 0.18f;

     x_off_dir = (x_off_dir * 2.0f - 1.0f) * 0.21f;
     y_off_dir = (y_off_dir * 2.0f - 1.0f) * 0.21f;
     
     glm::mat4 projection =
         glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, 0.01f, 10.0f);
     
     glm::mat4 view = glm::lookAt(glm::vec3(0.0f + x_off, 8.0f, 0.0f + y_off), 
                                  glm::vec3( 0.0f - x_off_dir, 0.0f,  0.0f - y_off_dir), 
                                  glm::vec3( 1.0f, 0.0f,  0.0f));

     
     
     lightSpaceMatrix = projection * view * glm::mat4(1.0f);


     
     glUniformMatrix4fv(glGetUniformLocation(depth_shader.program_id, "lightMatrix"), 1, GL_FALSE, glm::value_ptr(lightSpaceMatrix));

     glViewport(0, 0, 2048, 2048);
     test_depth_buffer1->Bind();
     glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

     for(auto i = 0; i < gameObjects.size(); i++) {
          drawGameobjectForShadow(gameObjects.at(i), depth_shader, i);
      }

     //drawGameobjectForShadow(gameObjects.at(0), depth_shader, 0);
      test_depth_buffer1->Unbind();

     // reset viewport
     glViewport(0, 0, screen_width, screen_height);
     glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
     
     
 }

GLuint StabilityPhase(GameObject& object, Shader shader) {

     glUseProgram(shader.program_id);
     
     CHECK_GL_ERROR();
     glm::mat4 model = glm::translate(glm::mat4(1.0f), object.position);
     model=glm::scale(model, object.scale);
     
     const glm::mat3 modelRotationMatrix = glm::mat3(
         model[0],
         model[1],
         model[2]
     );
     
     glm::mat3 normal_matrix = glm::transpose(glm::inverse(modelRotationMatrix));

     glUniformMatrix3fv(glGetUniformLocation(shader.program_id, "normal_matrix"), 1, GL_FALSE, glm::value_ptr(normal_matrix));
     CHECK_GL_ERROR();

     stability_fbo->Bind();
     object.get_geometry().get_vao().bind();

     CHECK_GL_ERROR();
     GLuint result;
     glGenTextures(1, &result);
     glBindTexture(GL_TEXTURE_2D, result);
     glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, screen_height, screen_height, 0, GL_RGBA, GL_FLOAT, NULL);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

     
     glActiveTexture(GL_TEXTURE0);
     glBindTexture(GL_TEXTURE_2D, object.getAccTexture());
     glUniform1i(glGetUniformLocation(shader.program_id, "accumulation_map"), 0);

     glActiveTexture(GL_TEXTURE1);
     glBindTexture(GL_TEXTURE_2D, object.get_geometry().texture.texture_id);
     glUniform1i(glGetUniformLocation(shader.program_id, "diffuse"), 1);
     
     CHECK_GL_ERROR();
     glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, result, 0);
     CHECK_GL_ERROR();
     
     glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
     glDrawElements(GL_TRIANGLES, 3 * object.get_geometry().triangles, GL_UNSIGNED_INT, 0);
     CHECK_GL_ERROR();
     glBindVertexArray(0);
     stability_fbo->Unbind();
     CHECK_GL_ERROR();
     glUseProgram(0);
     
     return result;
 }

void StabilityPhaseWrapper() {

     for(auto &o : gameObjects) {
         GLuint new_tex = StabilityPhase(o, *stability_shader);
         o.replaceAccTexture(new_tex);
     }
 }


void Draw() {
     
     glm::mat4 viewMatrix;
     glm::mat4 projectionMatrix;

     glm::vec3 camera_center = main_camera.position + main_camera.direction;
     
     viewMatrix = glm::lookAt(
         main_camera.position,
         camera_center,
         main_camera.camera_up
     );
     
     projectionMatrix = glm::perspective(glm::radians(60.0f), screen_width/(float)screen_height, 0.1f, 100.0f);
     
     CHECK_GL_ERROR();
     glUseProgram(main_shader->program_id);
     CHECK_GL_ERROR();
     
     glUniform3fv(glGetUniformLocation(main_shader->program_id, "camera_position"), 1, glm::value_ptr(main_camera.position));
     CHECK_GL_ERROR();
     
     
     // for(auto &object : gameObjects) {
     //     drawGameobject(object, *main_shader, viewMatrix, projectionMatrix);
     //     CHECK_GL_ERROR();
     // }

     for(auto i = 0; i < gameObjects.size(); i++) {
         drawGameobject(gameObjects.at(i), *main_shader, viewMatrix, projectionMatrix, i);
     }
     
     // VAO c_vao = initTestCube();
     // drawTestCube(c_vao, viewMatrix, projectionMatrix);
 }

void cursor_position_callback(GLFWwindow* window, double xpos, double ypos) {

     short Xmult = 0;
     short Ymult = 0;
     
     if (xpos - prev_cursor_x > 0) {
         Xmult = 1;
     } else if (xpos - prev_cursor_x < 0) {
         Xmult = -1;
     }

     if (ypos - prev_cursor_y > 0) {
         Ymult = 1;
     } else if (ypos - prev_cursor_y < 0) {
         Ymult = -1;
     }

     main_camera.Rotate(Ymult * .5f, Xmult * 0.5f);

     prev_cursor_x = xpos;
     prev_cursor_y = ypos;
 }

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
 {
     switch (key) {
         case GLFW_KEY_W: {
             if (action == GLFW_RELEASE) {
                 KEY_PRESSED[GameState::KEYS::MOVE_FORWARD] = false;
             } else {
                 KEY_PRESSED[GameState::KEYS::MOVE_FORWARD] = true;
                 main_camera.MoveStraight(1, 0);
             }
             break;
         }
         
         case GLFW_KEY_A: {
             if (action == GLFW_RELEASE) {
                 KEY_PRESSED[GameState::KEYS::MOVE_LEFT] = false;
             } else {
                 KEY_PRESSED[GameState::KEYS::MOVE_LEFT] = true;
                 main_camera.MoveSideays(1, 0);
             }
             break;
         }
        
         case GLFW_KEY_S: {
             if (action == GLFW_RELEASE) {
                KEY_PRESSED[GameState::KEYS::MOVE_BACK] = false;
             } else {
                 KEY_PRESSED[GameState::KEYS::MOVE_BACK] = true;
                 main_camera.MoveStraight(-1, 0);
             }
             break;
         }

         case GLFW_KEY_D: {
             if (action == GLFW_RELEASE) {
                 KEY_PRESSED[GameState::KEYS::MOVE_RIGHT] = false;
             } else {
                 KEY_PRESSED[GameState::KEYS::MOVE_RIGHT] = true;
                 main_camera.MoveSideays(-1, 0);
             }
             break;
         }
     }
 }


unsigned int quadVAO = 0;
unsigned int quadVBO;
void renderQuad()
{
    if (quadVAO == 0)
    {
        float quadVertices[] = {
            // positions        // texture Coords
            -1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
            -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
             1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
             1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
        };
        // setup plane VAO
        glGenVertexArrays(1, &quadVAO);
        glGenBuffers(1, &quadVBO);
        glBindVertexArray(quadVAO);
        glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    }
    glBindVertexArray(quadVAO);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glBindVertexArray(0);
}

void init_temp_accumulation_textures() {

    for(int i = 0; i < 8; i++) {
        glGenTextures(1, &temp_acc_map.at(i));
        glBindTexture(GL_TEXTURE_2D, temp_acc_map.at(i));
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, screen_height, screen_height, 0, GL_RGBA, GL_FLOAT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    }
    
}

void delete_temp_accumulation_textures() {

    for(int i = 0; i < 8; i++) {
        glDeleteTextures(1, &temp_acc_map.at(i));
    }
}

void init_acc_textures() {
    for(int i = 0; i < 8; i++) {
        glGenTextures(1, &accumulation_maps.at(i));
        glBindTexture(GL_TEXTURE_2D, accumulation_maps.at(i));
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, screen_height, screen_height, 0, GL_RGBA, GL_FLOAT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    }
}




int main(void)
{
    GLFWwindow* window;

    /* Initialize the library */
    if (!glfwInit())
        return -1;

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

     screen_width = screen_height;
     screen_height = screen_height;
     
     srand(static_cast <unsigned> (glfwGetTime()));
    /* Make the window's context current */
    glfwMakeContextCurrent(window);
    gladLoadGL();
     // if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
     //     std::cout << "Failed to initialize OpenGL context" << std::endl;
     //     return -1;
     // }

    // TextureManager::init();

    std::cout << "Current working dir of MAIN: " << std::filesystem::current_path() << std::endl;


     test_depth_buffer1 = new FBO;
     test_depth_buffer2 = new FBO;
    stability_fbo = new FBO;
     // main_shader = new Shader("./shaders/main.vert", "./shaders/main.frag");
     main_shader = new Shader("./shaders/main.vert", "./shaders/main.frag",
         "./shaders/main.tsc", "./shaders/main.tse");
    
     shadow_shader = new Shader("./shaders/depth.vert", "./shaders/depth.frag");

     debug_quad_shader = new Shader("./shaders/debugDepth.vert", "./shaders/debugDepth.frag");

     accum_shader = new Shader("./shaders/accum.vert", "./shaders/accum.frag","./shaders/accum.geom");

     stability_shader = new Shader("./shaders/stability.vert", "./shaders/stability.frag");
    
     CHECK_GL_ERROR();
     GameObject cube("./Gameobjects/objs/ground.obj",
         "./Gameobjects/textures/grass.jpg",
         "./Gameobjects/textures/Bricks_Terracotta_003_basecolor.jpg",
         glm::vec3(.0f), main_shader);
     cube.scale = glm::vec3(10, 1, 10);
     
     GameObject test_cube("./Gameobjects/objs/random_object.obj", 
         "./Gameobjects/textures/wall.jpg",
         "./Gameobjects/textures/Bricks_Terracotta_003_basecolor.jpg",
         glm::vec3(.0f), main_shader);

     GameObject cottage_obj("./Gameobjects/objs/cottage.obj",
         "./Gameobjects/textures/cottage.jpg",
         "./Gameobjects/textures/cottage.jpg",
         glm::vec3(.0f), main_shader);


    GameObject wired_sphere("./Gameobjects/objs/wired_sphere.obj",
        "./Gameobjects/textures/wired_sphere.jpg",
        "./Gameobjects/textures/bark.jpg",
        glm::vec3(.0f), main_shader
        );

    GameObject plane("./Gameobjects/objs/plane_w.obj",
       "./Gameobjects/textures/wall.jpg",
       "./Gameobjects/textures/wall.jpg",
       glm::vec3(.0f), main_shader
       );

    GameObject plane2("./Gameobjects/objs/plane_w.obj",
       "./Gameobjects/textures/wall.jpg",
       "./Gameobjects/textures/wall.jpg",
       glm::vec3(.0f), main_shader
       );

    GameObject plane3("./Gameobjects/objs/plane_w.obj",
       "./Gameobjects/textures/wall.jpg",
       "./Gameobjects/textures/wall.jpg",
       glm::vec3(.0f), main_shader
       );
    //occlusion_helper = new GameObject("./Gameobjects/objs/occlusion_1024.obj", "./Gameobjects/textures/Material_Base_color.jpg", "./Gameobjects/textures/Material_Base_color.jpg",glm::vec3(.0f), main_shader);
    
     test_cube.position = glm::vec3(0, 2, 0);
    cottage_obj.position = glm::vec3(-4, 3.2, 6.0f);
    cottage_obj.scale = glm::vec3(7,7,7);
    wired_sphere.position = glm::vec3(1,4,-3.0f);
    wired_sphere.scale = glm::vec3(2,2,2);

    plane.position = glm::vec3(4, 2, 0);
    plane2.position = glm::vec3(0, 2, 0);
    plane3.position = glm::vec3(-4, 2, 0);
    
    noiseTexture.createTexture("./Gameobjects/textures/noise.jpg");
    
     CHECK_GL_ERROR();
     // gameObjects.push_back(cube);
        gameObjects.push_back(plane);
    gameObjects.push_back(plane2);
    gameObjects.push_back(plane3);
     //gameObjects.push_back(cottage_obj);
    //   gameObjects.push_back(test_cube);
    //  
    // gameObjects.push_back(wired_sphere);
     // GameObject plane("C:/Users/mbudy/RiderProjects/SnowSurface/Gameobjects/objs/test_.obj",
     //     "C:/Users/mbudy/Documents/Adobe/Adobe Substance 3D Painter/export/cube.png",
     //     glm::vec3(2, 0, 2), main_shader);
     //
     // gameObjects.push_back(plane);
    /* Loop until the user closes the window */
     CHECK_GL_ERROR();

     glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
     glfwSetKeyCallback(window, key_callback);
     glfwSetCursorPosCallback(window, cursor_position_callback);
     glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // glEnable(GL_NORMALIZE);
     //glDisable(GL_CULL_FACE);
     glEnable(GL_DEPTH_TEST);
    glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);


    init_temp_accumulation_textures();
    initAccFBOs();
     initShadowmap();
        
    init_acc_textures();
    
    initStabFBO();
    Utils::Init();
    
    double current_frame = glfwGetTime();
    double last_frame = current_frame;
    double delta_time;

    screen_grid = new Grid();
    screen_grid->InitD(accum_shader);

    main_camera.position = glm::vec3(1.0f, 4.0f, 0.0f);
    

     //glFrontFace(GL_CW);
     while (!glfwWindowShouldClose(window))
    {

        
        current_frame = glfwGetTime();

         if (current_frame - last_frame >= 1.0f / 60.0f) {
             // init_temp_accumulation_textures();
             /* Render here */
             glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
             shadowPass(*shadow_shader);

             // init_temp_accumulation_textures();
             // initAccFBOs();
             //
             glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
             accumulation_pass(*accum_shader);
             
             for(auto i = 0; i < gameObjects.size(); i++) {
                 Utils::GaussianBlur(&temp_acc_map.at(i), &accumulation_maps.at(i));
             }

             // TextureManager::saveTexture(temp_acc_map.at(0), 1024, 1024, "blyat.png");
             // TextureManager::saveTexture(temp_acc_map.at(1), 1024, 1024, "blyat1.png");
             // TextureManager::saveTexture(temp_acc_map.at(2), 1024, 1024, "blyat2.png");


             for(auto i = 0; i < gameObjects.size(); i++) {
                Utils::Dilate(accumulation_maps.at(i), temp_acc_map.at(i));   
             }

             
             for(auto i = 0; i < gameObjects.size(); i++) {
                 GLuint t = Utils::Increment(temp_acc_map.at(i), gameObjects.at(i).getAccTexture());
             
                 gameObjects.at(i).replaceAccTexture(t);
             }
             
             
             for(auto i = 0; i < gameObjects.size(); i++) {
                 GLuint n = Utils::CalculateNormal(gameObjects.at(i).getAccTexture());
             
                 gameObjects.at(i).replaceNormalMap(n);
             }


             StabilityPhaseWrapper();
             
             Draw();
             // glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
             //  glUseProgram(debug_quad_shader->program_id);
             //  glUniform1f(glGetUniformLocation(debug_quad_shader->program_id, "near_plane"), 0.01f);
             //  glUniform1f(glGetUniformLocation(debug_quad_shader->program_id, "far_plane"), 30.0f);
             //  glActiveTexture(GL_TEXTURE0);
             //  glBindTexture(GL_TEXTURE_2D, accMap1);
             //  renderQuad();
             //  glUseProgram(0);
         
             /* Swap front and back buffers */
             glfwSwapBuffers(window);
             glfwPollEvents();
              //delete_temp_accumulation_textures();
             
         }
        
    }

     glfwDestroyWindow(window);
    glfwTerminate();
     
    return 0;
}

